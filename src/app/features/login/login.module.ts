import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SigninComponent } from './signin/signin.component';
import { MatCardModule } from '@angular/material/card';
import { SignupComponent } from './signup/signup.component';
import { RouterModule } from '@angular/router';
import { LoginRoutingModule } from './login-routing.module';
import { ForgetPasswordComponent } from './forget-password/forget-password.component';

@NgModule({
  declarations: [SigninComponent, SigninComponent, SignupComponent, ForgetPasswordComponent],
  imports: [CommonModule, MatCardModule, RouterModule, LoginRoutingModule],
  exports: [SigninComponent, SignupComponent, ForgetPasswordComponent],
})
export class LoginModule {}
